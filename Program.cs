﻿using Linq.Extensions;

namespace LinqExtensions;

internal class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("--- элементы последовательности [-20,20] с нечетными индексами ---");
        foreach (var i in Enumerable.Range(-20, 41).TakeOdd())
            Console.WriteLine(i);


        Console.WriteLine("---- элементы последовательности [-20,20] с четными индексами ----");
        foreach (var i in Enumerable.Range(-20, 41).TakeEven())
            Console.WriteLine(i);


        Console.WriteLine("------------------ отрицательные от -20 до 20 --------------------");
        foreach (var i in Enumerable.Range(-20, 41).TakeNegative())
            Console.WriteLine(i);

        Console.WriteLine("------------------ положительные от -20 до 20 --------------------");
        foreach (var i in Enumerable.Range(-20, 41).TakePositive())
            Console.WriteLine(i);

        var rnd = new Random();

        List<double> sList = new List<double>();
        for (double i = 0; i < 50; i += 0.05)
            sList.Add(Math.Sin(i) + rnd.Next(-1,2) * rnd.NextDouble()*3);

        Console.WriteLine("------------- синус с отклонениями --------------");

        foreach (var i in sList)
            Console.WriteLine(i);

        Console.WriteLine("--------------- сглаженный синус ----------------");

        foreach (var i in sList.SmoothByMovingAverage(20))
            Console.WriteLine(i);


        List<int> ilist = new List<int>();
        for (int i = 0; i < 50; i++)
            ilist.Add(i + rnd.Next(-10, 11));

        Console.WriteLine("-------- растущая прямая с отклонениями ---------");

        foreach (var i in ilist)
            Console.WriteLine(i);

        Console.WriteLine("---------- сглаженная растущая прямая -----------");

        foreach (var i in ilist.SmoothByMovingAverage(5))
            Console.WriteLine(i);

    }
}
