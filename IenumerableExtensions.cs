﻿using System.Numerics;

namespace Linq.Extensions;

public static class IEnumerableExtensions
{
    /// <summary>
    /// возвращает последовательность, содержащую нечётные по порядку элементы исходной
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности</typeparam>
    /// <param name="enumerable">Исходная последовательность</param>
    public static IEnumerable<T> TakeOdd<T>(this IEnumerable<T> enumerable)
    {
        int i = 0;
        if (enumerable == null)
            throw new ArgumentNullException(nameof(enumerable));

        foreach (var e in enumerable)
            if (++i % 2 == 1)
                yield return e;
    }

    /// <summary>
    /// возвращает последовательность, содержащую чётные по порядку элементы исходной
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности</typeparam>
    /// <param name="enumerable">Исходная последовательность</param>
    public static IEnumerable<T> TakeEven<T>(this IEnumerable<T> enumerable)
    {
        int i = 0;
        if (enumerable == null)
            throw new ArgumentNullException(nameof(enumerable));

        foreach (var e in enumerable)
            if (++i % 2 == 0)
                yield return e;
    }

    /// <summary>
    /// возвращает последовательность, содержащую отрицательные элементы исходной
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности, числовой тип</typeparam>
    /// <param name="enumerable">Исходная последовательность</param>
    public static IEnumerable<T> TakeNegative<T>(this IEnumerable<T> enumerable) where T : INumber<T>
    {
        if (enumerable == null)
            throw new ArgumentNullException(nameof(enumerable));

        foreach (var e in enumerable)
            if (T.IsNegative(e))
                yield return e;
    }

    /// <summary>
    /// возвращает последовательность, содержащую положительные элементы исходной
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности, числовой тип</typeparam>
    /// <param name="enumerable">Исходная последовательность</param>
    /// <remarks>Ноль не входит в выходую последовательность</remarks>
    public static IEnumerable<T> TakePositive<T>(this IEnumerable<T> enumerable) where T : INumber<T>
    {
        if (enumerable == null)
            throw new ArgumentNullException(nameof(enumerable));

        foreach (var e in enumerable)
            if (T.IsPositive(e) && e.CompareTo(T.Zero) != 0)
                yield return e;
    }

    /// <summary>
    /// сглаживает элементы числовой последовательности алгоритмом скользящего среднего
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности, числовой тип</typeparam>
    /// <param name="enumerable">Исходная последовательность</param>
    /// <param name="width">ширина окна скользящего среднего</param>
    /// <remarks>
    /// Значение элемента последовательности-результата равно среднему арифметическому значений исходной последовательности в окрестности <paramref name="width"/> этого элемента
    /// </remarks>
    public static IEnumerable<double> SmoothByMovingAverage<T>(this IEnumerable<T> enumerable, int width) where T : INumber<T>
    {
        if (enumerable == null)
            throw new ArgumentNullException(nameof(enumerable));

        Queue<T> queue = new Queue<T>(width * 2);

        foreach (var e in enumerable)
        {
            queue.Enqueue(e);

            if (queue.Count >= width)
                yield return queue.Average<T, double>();

            if (queue.Count == width * 2)
                queue.Dequeue();
        }

        while (queue.Count > width)
        {
            queue.Dequeue();
            yield return queue.Average<T, double>();
        }
    }

    /// <summary>
    /// Сумма элементов последовательности INumber
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности, числовой тип</typeparam>
    /// <param name="enumerable">Последовательность</param>
    /// <remarks>Что-то "готовое" не получилось использовать...</remarks>
    public static T Sum<T>(this IEnumerable<T> enumerable) where T : INumber<T>
    {
        T sum = T.Zero;
        
        foreach (var value in enumerable)
            sum += value;

        return sum;
    }

    /// <summary>
    /// Среднее элементов последовательности INumber
    /// </summary>
    /// <typeparam name="T">Тип элеменов последовательности, числовой тип</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <param name="enumerable">Последовательность</param>
    /// <remarks>Что-то "готовое" не получилось использовать...</remarks>
    static TResult Average<T, TResult>(this IEnumerable<T> enumerable)
        where T : INumber<T>
        where TResult : INumber<TResult>
    {
        return TResult.CreateChecked(enumerable.Sum() / T.CreateChecked(enumerable.Count()));
    }
}
